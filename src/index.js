import "./assets/css/styles.css";

import * as React from "react";
import * as ReactDOM from "react-dom";
import {MainComponent} from "./component/MainComponent";

ReactDOM.render(<MainComponent/>, document.getElementById("app"));